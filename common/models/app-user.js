'use strict';

module.exports = function(Appuser) {
	/*Start of remothe methods for app*/
	Appuser.remoteMethod('getDashboard', {
		'http': {
            verb: 'post',
            path: '/getDashboard'
        },
        accepts: [
            {arg: 'userId', type: 'string', required:true},
            {arg: 'token', type: 'string', required: true},
            {arg: 'role', type: 'string', required: true}
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
	});

  Appuser.remoteMethod('getTotalsPeriod', {
    'http': {
            verb: 'post',
            path: '/getTotalsPeriod'
        },
        accepts: [
            {arg: 'userId', type: 'string', required:true},
            {arg: 'token', type: 'string', required: true},
            {arg: 'role', type: 'string', required: true},
            {arg: 'period', type: 'string', required: true}
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
  });


	//*** REMOTE METHODS FUNCTIONS ***//
	/**
		API- para dashboard de Director
		recibe:  {
							userId - string,
						 	token - token de usuario logeado
						 }
		retorna: data - object
	**/
  Appuser.getDashboard = function (userId,token,role,callback) {
  	var relation = '';
  	var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).toJSON();
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).toJSON();
    var biweek = new Date(date.getFullYear(), date.getMonth(), 16).toJSON();
    var today = date.toJSON();
    var mondays = getMondays(date);
    var base = getBase(today,firstDay,mondays);
    var response = {
    	"totalWeek1" : 0,
    	"totalWeek2" : 0,
    	"totalWeek3" : 0,
    	"totalWeek4" : 0,
    	"totalbiweek1": 0,
    	"totalbiweek2": 0,
    	"totalMonth" : 0,
    	"totalDiverse":0
    };

    console.log(biweek);
    /*Dependiento el rol del usuario se general la Query*/
	  switch(role){
	  	case 'Director':
	  	 relation= 'direction';
	  	break;
	  	case 'Gerente':
	  	 relation= 'manager';
	  	break;
	  	case 'Jefe':
	  	 relation= 'headship';
	  	break;
	  }

	  Appuser.findById(userId,{
		  include: {
		  	relation : relation,
		  		scope:{
		  			include :{
		  				relation : 'activities',
		  				scope:{
		  					where: {
		  						and:[ {startDate: {gt: firstDay}} ,{startDate: {lt: lastDay} }]
                },
                include : 'task' 
		  				}
		  			}
		  		}
		  	},
		  	fields: ['name', 'rol'],
	  	},
		  function(err,user){
				if (err) {
		      console.log(err);
		      callback(err);
		    } else {

          if(role == 'Director'){
            if(typeof user.direction() != "undefined" || typeof user.direction() != null){
               user.direction().activities().forEach(function (task, index, arr){
               // console.log("Tareas" , task);
                switch(task.task().periodicity){
                case 'Semanal' : 
                  if((new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() <= new Date(mondays[0]).getTime()) || 
                    (new Date(task.startDate).getTime() >= new Date(mondays[0]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime())){ 
                    if(task.compleated){
                        response.totalWeek1 = response.totalWeek1 + task.task().weighing;
                    }
                  }
                else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                   if(task.compleated){
                        response.totalWeek2 = response.totalWeek2 + task.task().weighing;
                    }
                }
                 else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime() ){
                   if(task.compleated){
                        response.totalWeek3 = response.totalWeek3 + task.task().weighing;
                    }
                }
               else if(new Date(task.startDate).getTime() >= new Date(mondays[5]).getTime()){
                   if(task.task().compleated){
                        response.totalWeek4 = response.totalWeek4 + task.task().weighing;
                    }
                }   
                break;

                case 'Quincenal':
                  if(new Date(task.startDate).getTime() > new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                    if(task.compleated){
                      response.totalbiweek1 =  response.totalbiweek1 + task.task().weighing;
                    }
                  }
                  else if(new Date(task.startDate).getTime() >=  new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < lastDay){
                     if(task.compleated){
                           response.totalbiweek2 =  response.totalbiweek2 + task.task().weighing;
                      }
                  }
                break;

                case 'Mensual':
                  if(task.compleated){
                        response.totalMonth = response.totalMonth + task.task().weighing;
                  }
                break;

                case 'Diversas':
                  if(task.compleated){
                        response.totalDiverse = response.totalDiverse + task.task().weighing;
                  }
                break;
              }
            });
            }
             
          }

          else if(role == 'Gerente'){
            if(typeof user.management() != "undefined" || typeof user.management() != null){
               user.management().activities().forEach(function (task, index, arr) {
              //console.log("Tareas" , task);
              switch(task.task().periodicity){
                case 'Semanal' : 
                  if((new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() <= new Date(mondays[0]).getTime()) || 
                    (new Date(task.startDate).getTime() >= new Date(mondays[0]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime())){ 
                    if(task.compleated){
                        response.totalWeek1 = response.totalWeek1 + task.task().weighing;
                    }
                  }
                else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                   if(task.compleated){
                        response.totalWeek2 = response.totalWeek2 + task.task().weighing;
                    }
                }
                 else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime() ){
                   if(task.compleated){
                        response.totalWeek3 = response.totalWeek3 + task.task().weighing;
                    }
                }
               else if(new Date(task.startDate).getTime() >= new Date(mondays[5]).getTime()){
                   if(task.task().compleated){
                        response.totalWeek4 = response.totalWeek4 + task.task().weighing;
                    }
                }   
                break;

                case 'Quincenal':
                  if(new Date(task.startDate).getTime() > new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                    if(task.compleated){
                      response.totalbiweek1 =  response.totalbiweek1 + task.task().weighing;
                    }
                  }
                  else if(new Date(task.startDate).getTime() >=  new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < lastDay){
                     if(task.compleated){
                           response.totalbiweek2 =  response.totalbiweek2 + task.task().weighing;
                      }
                  }
                break;

                case 'Mensual':
                  if(task.compleated){
                        response.totalMonth = response.totalMonth + task.task().weighing;
                  }
                break;

                case 'Diversas':
                  if(task.compleated){
                        response.totalDiverse = response.totalDiverse + task.task().weighing;
                  }
                break;
              }
            });
            }
           
          }

          else if(role == 'Jefe'){
            if(typeof user.headship() != "undefined" || typeof user.headship() != null){
                user.headship().activities().forEach(function (task, index, arr) {
              //console.log("Tareas" , task);
              switch(task.task().periodicity){
                case 'Semanal' : 
                  if((new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() <= new Date(mondays[0]).getTime()) || 
                    (new Date(task.startDate).getTime() >= new Date(mondays[0]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime())){ 
                    if(task.compleated){
                        response.totalWeek1 = response.totalWeek1 + task.task().weighing;
                    }
                  }
                else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                   if(task.compleated){
                        response.totalWeek2 = response.totalWeek2 + task.task().weighing;
                    }
                }
                 else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime() ){
                   if(task.compleated){
                        response.totalWeek3 = response.totalWeek3 + task.task().weighing;
                    }
                }
               else if(new Date(task.startDate).getTime() >= new Date(mondays[5]).getTime()){
                   if(task.task().compleated){
                        response.totalWeek4 = response.totalWeek4 + task.task().weighing;
                    }
                }   
                break;

                case 'Quincenal':
                  if(new Date(task.startDate).getTime() > new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()){
                    if(task.compleated){
                      response.totalbiweek1 =  response.totalbiweek1 + task.task().weighing;
                    }
                  }
                  else if(new Date(task.startDate).getTime() >=  new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < lastDay){
                     if(task.compleated){
                           response.totalbiweek2 =  response.totalbiweek2 + task.task().weighing;
                      }
                  }
                break;

                case 'Mensual':
                  if(task.compleated){
                        response.totalMonth = response.totalMonth + task.task().weighing;
                  }
                break;

                case 'Diversas':
                  if(task.compleated){
                        response.totalDiverse = response.totalDiverse + task.task().weighing;
                  }
                break;
              }
            });
            }
          
          }
		   
		    	/** obtenemos la semana actual*/
          if(new Date(today).getTime() >= new Date(firstDay).getTime() && new Date(today).getTime() < new Date(mondays[1]).getTime()){
              
              response.totalWeek = response.totalWeek1;    
          }
          else if(new Date(today).getTime() >= new Date(mondays[1]).getTime() && new Date(today).getTime() < new Date(mondays[2]).getTime()){
              response.totalWeek = response.totalWeek2; 
              
          }
          else if(new Date(today).getTime() >= new Date(mondays[2]).getTime() && new Date(today).getTime() < new Date(mondays[3]).getTime()){
              response.totalWeek = response.totalWeek3;  
              
          }
          else if(new Date(today).getTime() >= new Date(mondays[3]).getTime() && new Date(today).getTime() < new Date(lastDay).getTime()){
              response.totalWeek = response.totalWeek4;        
          }

          /*obtenemos la quincena actual*/
          if(new Date(today).getTime() >= new Date(firstDay).getTime() && new Date(today).getTime() <= new Date(biweek).getTime()){
            response.totalBiweek = response.totalbiweek1;    
          }
          else if( new Date(today).getTime() >= new Date(biweek).getTime() &&  new Date(today).getTime() <= new Date(lastDay).getTime()){
          	response.totalBiweek = response.totalbiweek2;
          }

          response.generalTotal = (response.totalWeek1 + response.totalWeek2	+ response.totalWeek3	+ response.totalWeek4	+ response.totalbiweek1 + response.totalbiweek2 + response.totalMonth) / base * 100;
		    }
		    callback(null, response);
		  });
 };

  function getMondays(date){
          var month = date.getMonth();
          var mondays = [];
          date.setDate(1);
          date.setHours(0,0,0,0);
          // Obtenemos el primer mes del mes
          while (date.getDay() !== 1) {
              date.setDate(date.getDate() + 1)
              date.setHours(0,0,0,0);
          }
          // los otros lunes del mes
          while (date.getMonth() === month) {
              mondays.push(new Date(date.getTime()).toJSON());
              date.setDate(date.getDate() + 7)
              date.setHours(0,0,0,0);
          }
          return mondays;
  }

  function getBase(today,firstDay,mondays){
      var date = new Date();
      var base = 0;
      //$log.log("numero", date.getTime());
    //new Date(vm.today).getDay() > new Date(vm.mondays[2]).getDay() &&
      if(new Date(today).getTime() > new Date(firstDay).getTime() && new Date(today).getTime() < new Date(mondays[1]).getTime() ){
          base = 300;
      }
      else if(new Date(today).getTime() < new Date(mondays[2]).getTime()){
            base = 400;
      }
      else if(new Date(today).getTime() > new Date(mondays[2]).getTime() && new Date(today).getTime() < new Date(mondays[3]).getTime()){
            base = 600;
      }
      else if(today > mondays[3]){
            base = 700;
      }

      console.log("Base ",base);
      return base;
  }

  Appuser.getTotalsPeriod = function (userId,token,role,period,callback) {
    var date = new Date();
    var today = date.toJSON();
    var counter = 0;
    var relation = "";
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).toJSON();
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).toJSON();
    var biweek = new Date(date.getFullYear(), date.getMonth(), 16).toJSON();
    var mondays = getMondays(date);
    var response = {
      'total' : 0,
      'activities' : []
    };

     switch(role){
      case 'Director':
       relation = 'direction';
      break;
      case 'Gerente':
       relation = 'management';
      break;
      case 'Jefe':
       relation = 'headship';
      break;
    }

    console.log(relation);

    Appuser.findById(userId,{
      include: {
        relation : relation,
          scope:{
            include :{
              relation : 'activities',
              scope:{
                where: {
                  and:[ {startDate: {gt: firstDay}} ,{startDate: {lt: lastDay}}]
                },
                include : 'task' 
              }
            }
          }
        },
        fields: ['name', 'rol'],
      },
      function(err,user){
        if (err){
          console.log(err);
          callback(err);
        }else {
          switch(role){
            case 'Director' :
              user.direction().activities().forEach(function (task, index, arr){ 
                switch(period){
                  case 'Semanal':
                    if(task.task().periodicity == period){  
                      if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime()
                      && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[1]).getTime() ){
                        if(task.compleated){
                          response.total = response.total + task.task().weighing;
                        }
                        response.activities.push(task);      
                      }
                      //**Caso semana 2 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()
                        && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[2]).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);     
                      }
                      //**Caso semana 3 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime()
                          && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[3]).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                      }
                      //**Caso semana 4 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[3]).getTime() && new Date(task.startDate).getTime() < new Date(lastDay).getTime()
                                && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(lastDay).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);       
                      }      
                    }
                  break;

                  case 'Quincenal':
                    if(task.task().periodicity == period){
                      /* Caso primer quincena*/
                        if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(biweek).getTime()
                            && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(biweek).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                        }
                        //**Caso semana 2 **//
                        else if(new Date(task.startDate).getTime() >= new Date(biweek).getTime() && new Date(today).getTime() > new Date(task.startDate).getTime()
                          && new Date(today).getTime() < new Date(lastDay).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);              
                        }
                    }  
                  break;

                  case 'Mensual':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;

                  case 'Diversas':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;
                }
              });  
            break;

            case 'Gerente':
              user.management().activities().forEach(function(task,index,arr){
                switch(period){
                  case 'Semanal':
                    if(task.task().periodicity == period){  
                      if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime()
                      && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[1]).getTime() ){
                        if(task.compleated){
                          response.total = response.total + task.task().weighing;
                        }
                        response.activities.push(task);      
                      }
                      //**Caso semana 2 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()
                        && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[2]).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);     
                      }
                      //**Caso semana 3 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime()
                          && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[3]).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                      }
                      //**Caso semana 4 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[3]).getTime() && new Date(task.startDate).getTime() < new Date(lastDay).getTime()
                                && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(lastDay).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);       
                      }      
                    }
                  break;

                  case 'Quincenal':
                    if(task.task().periodicity == period){
                      /* Caso primer quincena*/
                        if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(biweek).getTime()
                            && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(biweek).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                        }
                        //**Caso semana 2 **//
                        else if(new Date(task.startDate).getTime() >= new Date(biweek).getTime() && new Date(today).getTime() > new Date(task.startDate).getTime()
                          && new Date(today).getTime() < new Date(lastDay).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);              
                        }
                    }  
                  break;

                  case 'Mensual':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;

                  case 'Diversas':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;
                }
              });
            break;

            case 'Jefe':
              user.headship().activities().forEach(function(task,index,arr){
                 switch(period){
                  case 'Semanal':
                    if(task.task().periodicity == period){  
                      if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(mondays[1]).getTime()
                      && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[1]).getTime() ){
                        if(task.compleated){
                          response.total = response.total + task.task().weighing;
                        }
                        response.activities.push(task);      
                      }
                      //**Caso semana 2 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[1]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[2]).getTime()
                        && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[2]).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);     
                      }
                      //**Caso semana 3 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[2]).getTime() && new Date(task.startDate).getTime() < new Date(mondays[3]).getTime()
                          && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(mondays[3]).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                      }
                      //**Caso semana 4 **//
                      else if(new Date(task.startDate).getTime() >= new Date(mondays[3]).getTime() && new Date(task.startDate).getTime() < new Date(lastDay).getTime()
                                && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(lastDay).getTime() ){
                        if(task.compleated)
                          response.total = response.total + task.task().weighing;
                        response.activities.push(task);       
                      }      
                    }
                  break;

                  case 'Quincenal':
                    if(task.task().periodicity == period){
                      /* Caso primer quincena*/
                        if(new Date(task.startDate).getTime() >= new Date(firstDay).getTime() && new Date(task.startDate).getTime() < new Date(biweek).getTime()
                            && new Date(today).getTime() > new Date(task.startDate).getTime() && new Date(today).getTime() < new Date(biweek).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);        
                        }
                        //**Caso semana 2 **//
                        else if(new Date(task.startDate).getTime() >= new Date(biweek).getTime() && new Date(today).getTime() > new Date(task.startDate).getTime()
                          && new Date(today).getTime() < new Date(lastDay).getTime() ){
                            if(task.compleated)
                              response.total = response.total + task.task().weighing;
                            response.activities.push(task);              
                        }
                    }  
                  break;

                  case 'Mensual':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;

                  case 'Diversas':
                    if(task.task().periodicity == period){
                      if(task.compleated)
                        response.total = response.total + task.task().weighing;
                          response.activities.push(task);  
                    }
                  break;
                }
              });
            break;
          }
        /** AQUI VA TODO EL CODIGO**/
        callback(null,response);
        }
      });
    }
};
