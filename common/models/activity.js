'use strict';

module.exports = function(Activity) {
	/*Start of remothe methods for app*/
	Activity.remoteMethod('checkTask', {
		'http': {
            verb: 'post',
            path: '/checkTask'
        },
        accepts: [
            {arg: 'activityId', type: 'string', required:true},
            {arg: 'token', type: 'string', required: true}
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
	});

	//*** REMOTE METHODS FUNCTIONS ***//
	/**
		method- para guardado de estatus de las tareas
		recibe: {
					token - token de usuario logeado,
					activityid - string	
				}
		retorna: data - object
	**/
	Activity.checkTask = function (activityId,token,callback) {
		var date = new Date();
		 Activity.findById(activityId, 
		 	function(err,activity){
				if (err) {
		      		console.log(err);
		      		callback(err);
		    	} else {
		    		console.log("la actividad",activity);
		    		var status = !activity.compleated;
		    		activity.updateAttributes({compleated: status,endDate:date.toJSON()},function(){
              		});	
              		callback(null,activity);
		    	}
			});

	};
};